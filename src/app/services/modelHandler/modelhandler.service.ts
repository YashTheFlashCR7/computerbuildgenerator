/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
//datamodels
import { component } from "../../models/component.model";

@Injectable()
export class modelhandlerService {
    computerComponents = [{
        id: 'CASE', image: 'case-image', title: 'Case',
        subtitle: 'The enclosure that contains most of the components of a personal computer',
        content: 'Cases can come in many different sizes (known as form factors).The size and shape of a computer case is usually determined by the form factor of the motherboard, since it is the largest component of most computer',
        Addcomponents: [ ],
        average:0,
    },
    {
        id: 'PSU', image: 'psu-image', title: 'Power supply',
        subtitle: 'The enclosure that contains most of the components of a personal computer',
        content: 'Cases can come in many different sizes (known as form factors). The size and shape of a computer case is usually determined by the form factor of the motherboard, since it is the largest component of most computer',
        Addcomponents: [],
        average:0,
    },
    {
        id: 'MB', image: 'motherboard-image', title: 'Motherboard',
        subtitle: 'The enclosure that contains most of the components of a personal computer',
        content: 'Cases can come in many different sizes (known as form factors). The size and shape of a computer case is usually determined by the form factor of the motherboard, since it is the largest component of most computer',
        Addcomponents: [],
        average:0,
    },
    {
        id: 'HDD', image: 'storage-image', title: 'Storage',
        subtitle: 'The enclosure that contains most of the components of a personal computer',
        content: 'Cases can come in many different sizes (known as form factors). The size and shape of a computer case is usually determined by the form factor of the motherboard, since it is the largest component of most computer',
        Addcomponents: [],
        average:0,
    },
    {
        id: 'RAM', image: 'ram-image', title: 'Random Access Memory (RAM)',
        subtitle: 'The enclosure that contains most of the components of a personal computer',
        content: 'Cases can come in many different sizes (known as form factors). The size and shape of a computer case is usually determined by the form factor of the motherboard, since it is the largest component of most computer',
        Addcomponents: [],
        average:0,
    },
    {
        id: 'GPU', image: 'graphicscard-image', title: 'Graphics card',
        subtitle: 'The enclosure that contains most of the components of a personal computer',
        content: 'Cases can come in many different sizes (known as form factors). The size and shape of a computer case is usually determined by the form factor of the motherboard, since it is the largest component of most computer',
        Addcomponents: [],
        average:0,
    },
    {
        id: 'CPUCOOLER', image: 'cpucooler-image', title: 'CPU cooler',
        subtitle: 'The enclosure that contains most of the components of a personal computer',
        content: 'Cases can come in many different sizes (known as form factors). The size and shape of a computer case is usually determined by the form factor of the motherboard, since it is the largest component of most computer',
        Addcomponents: [],
        average:0,
    },]

    UnitcomputerComponents = [{
        id: 'CASE', image: 'case-image', title: 'Case',
        subtitle: 'The enclosure that contains most of the components of a personal computer',
        content: 'Cases can come in many different sizes (known as form factors).The size and shape of a computer case is usually determined by the form factor of the motherboard, since it is the largest component of most computer',
        Addcomponents: [ 
            {componentText: 'COOLERMASTER MASTERCASE H500P MESH GAMING CASE',ComponentPrice: 2201},
            {componentText: 'THERMALTAKE VIEW 37 RIING EDITION',ComponentPrice: 2875},
            {componentText: 'THERMALTAKE VIEW 71 TG SNOW EDITION',ComponentPrice: 2891},
            {componentText: 'Lian-Li PC-O11 Dynamic ATX Mid-Tower Chassis (White)',ComponentPrice: 2901},],
        average:0,
    },
    {
        id: 'PSU', image: 'psu-image', title: 'Power supply',
        subtitle: 'The enclosure that contains most of the components of a personal computer',
        content: 'Cases can come in many different sizes (known as form factors). The size and shape of a computer case is usually determined by the form factor of the motherboard, since it is the largest component of most computer',
        Addcomponents: [
        {componentText: 'CORSAIR RM850x',ComponentPrice: 1809},
        {componentText: 'Corsair CX Series CXM750 750W High Performance Modular Power Supply',ComponentPrice: 1909},
        {componentText: 'THERMALTAKE TOUGHPOWER GRAND RGB ATX POWER SUPPLY UNIT (850W) (BLACK)',ComponentPrice: 2197},],
        average:0,
    },
    {
        id: 'MB', image: 'motherboard-image', title: 'Motherboard',
        subtitle: 'The enclosure that contains most of the components of a personal computer',
        content: 'Cases can come in many different sizes (known as form factors). The size and shape of a computer case is usually determined by the form factor of the motherboard, since it is the largest component of most computer',
        Addcomponents: [
        {componentText: 'ASROCK Z390 PRO4 SOCKET-1151-V2 ATX 8TH GEN MOTHERBOARD',ComponentPrice: 1809},
        {componentText: 'MSI Z390-A PRO INTEL MOTHERBOARD',ComponentPrice: 2999},
        {componentText: 'GIGABYTE AORUS Z390 PRO INTEL 9TH GEN WIFI ATX GAMING MOTHERBOARD',ComponentPrice: 3699}],
        average:0,
    },
    {
        id: 'HDD', image: 'storage-image', title: 'Storage',
        subtitle: 'The enclosure that contains most of the components of a personal computer',
        content: 'Cases can come in many different sizes (known as form factors). The size and shape of a computer case is usually determined by the form factor of the motherboard, since it is the largest component of most computer',
        Addcomponents: [ 
        {componentText: 'Samsung 970 EVO Plus 2TB M.2 PCI-e Gen 3.0 x 4',ComponentPrice: 1609},
        {componentText: 'Samsung 970 EVO Plus 1TB M.2 PCI-e Gen 3.0 x 4',ComponentPrice: 3599},
        {componentText: 'Samsung 970 EVO SSD',ComponentPrice: 4699}],
        average:0,
    },
    {
        id: 'RAM', image: 'ram-image', title: 'Random Access Memory (RAM)',
        subtitle: 'The enclosure that contains most of the components of a personal computer',
        content: 'Cases can come in many different sizes (known as form factors). The size and shape of a computer case is usually determined by the form factor of the motherboard, since it is the largest component of most computer',
        Addcomponents: [
            {componentText: 'CORSAIR CMW16GX4M2C3200C16W VENGEANCE RGB PRO 16GB (2X8GB)',ComponentPrice: 1809},
            {componentText: 'HyperX Predator DDR4 RGB',ComponentPrice: 1999},
            {componentText: 'CORSAIR Dominator Platinum RGB 16GB DDR4 DRAM',ComponentPrice: 2699}
        ],
        average:0,
    },
    {
        id: 'GPU', image: 'graphicscard-image', title: 'Graphics card',
        subtitle: 'The enclosure that contains most of the components of a personal computer',
        content: 'Cases can come in many different sizes (known as form factors). The size and shape of a computer case is usually determined by the form factor of the motherboard, since it is the largest component of most computer',
        Addcomponents: [
            {componentText: 'NVIDIA GEFORCE RTX 2070',ComponentPrice: 5809},
            {componentText: 'NVIDIA GEFORCE RTX 2060',ComponentPrice: 6999},
            {componentText: 'EVGA GEFORCE GTX 1660 TI 6GB',ComponentPrice: 7699}
        ],
        average:0,
    },
    {
        id: 'CPUCOOLER', image: 'cpucooler-image', title: 'CPU cooler',
        subtitle: 'The enclosure that contains most of the components of a personal computer',
        content: 'Cases can come in many different sizes (known as form factors). The size and shape of a computer case is usually determined by the form factor of the motherboard, since it is the largest component of most computer',
        Addcomponents: [
            {componentText: 'Thermaltake Floe Riing Cooling',ComponentPrice: 1809},
            {componentText: 'NZXT Kraken X42',ComponentPrice: 2999},
            {componentText: 'Corsair hydro h150i pro 360mm',ComponentPrice: 3699}
        ],
        average:0,
    },]

    build=[
        {BuildID: 'COST1', 
        BuildDefinition: 'Cheapest build', 
        image:'pc-image',
        Components:[{componentText: 'NVIDIA GEFORCE RTX 2060',ComponentPrice: 6799,image: 'case-image'},
                    {componentText: 'Lian-Li PC-O11 Dynamic ATX Mid-Tower Chassis (White)',ComponentPrice: 2482,image: 'case-image'},
                    {componentText: 'CORSAIR CMW16GX4M2C3200C16W VENGEANCE RGB PRO 16GB (2X8GB) DDR4-3200MHZ CL16 1.35V 288-PIN WHITE DESKTOP MEMORY',ComponentPrice: 2495,image: 'case-image'},
                    {componentText: 'Corsair CX Series CXM750 750W High Performance Modular Power Supply',ComponentPrice: 1809,image: 'case-image'},
                    {componentText: 'GIGABYTE AORUS Z390 PRO INTEL 9TH GEN WIFI ATX GAMING MOTHERBOARD',ComponentPrice: 3999,image: 'case-image'},
                    {componentText: 'INTEL CORE I5-8400 PROCESSOR (9M CACHE, UP TO 4.00 GHZ)',ComponentPrice: 3719,image: 'case-image'}], 
        Rating: 5,
        Total: 18821,
        ComponentAverage:5000,
        budget6months: 4000,
        budget12months: 3000},
        {BuildID: 'COST2', 
        BuildDefinition: 'Expensive build', 
        image:'pc-image',
        Components:[{componentText: 'NVIDIA GEFORCE RTX 2060',ComponentPrice: 6799,image: 'case-image'},
                    {componentText: 'Lian-Li PC-O11 Dynamic ATX Mid-Tower Chassis (White)',ComponentPrice: 2482,image: 'case-image'},
                    {componentText: 'CORSAIR CMW16GX4M2C3200C16W VENGEANCE RGB PRO 16GB (2X8GB) DDR4-3200MHZ CL16 1.35V 288-PIN WHITE DESKTOP MEMORY',ComponentPrice: 2495,image: 'case-image'},
                    {componentText: 'Corsair CX Series CXM750 750W High Performance Modular Power Supply',ComponentPrice: 1809,image: 'case-image'},
                    {componentText: 'GIGABYTE AORUS Z390 PRO INTEL 9TH GEN WIFI ATX GAMING MOTHERBOARD',ComponentPrice: 3999,image: 'case-image'},
                    {componentText: 'INTEL CORE I5-8400 PROCESSOR (9M CACHE, UP TO 4.00 GHZ)',ComponentPrice: 3719,image: 'case-image'}], 
        Rating: 5,
        Total: 18821,
        ComponentAverage:5000,
        budget6months: 4000,
        budget12months: 3000}]

    createComputerComponentDataModel() {
        let component: any = []
        this.computerComponents.forEach(element => {
            component.push({
                id: element['id'],
                image: element['image'],
                title: element['title'],
                subtitle: element['subtitle'],
                content: element['content'],
                Addcomponents: element['Addcomponents'],
                average:element['average'],
            })
        });
        return component;
    }

    createBuildDataModel(){
        return this.build;
    }

    createUnitTestModel(){
        return this.UnitcomputerComponents
    }

    createComponentBuildModel(){
        let componetBuildModel =[{BuildID: '', 
        BuildDefinition: '', 
        Components:[{componentText: '',ComponentPrice: 0,image: ''}]}]

        return componetBuildModel;
    }

}
