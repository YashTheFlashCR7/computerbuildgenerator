import {JsonProperty, JsonObject} from '../lib/tj.deserializer'

@JsonObject
export class component {
  @JsonProperty('id', String, true)
  public id: string = undefined;

  @JsonProperty('image', String, true)
  public image: string = undefined;

  @JsonProperty('title', String, true)
  public title: string = undefined;

  @JsonProperty('subtitle', String, true)
  public subtitle: string = undefined;

  @JsonProperty('content', String, true)
  public content: string = undefined;

}