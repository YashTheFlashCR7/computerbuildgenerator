import { PageNotFoundComponent } from '../not-found.component';
import { LayoutComponent } from '../layout/layout.component';
import { ImgSrcDirective } from '../directives/imgSrc.directive';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_INITIALIZER } from '@angular/core';
import { NDataSourceService } from '../n-services/n-dataSorce.service';
import { environment } from '../../environments/environment';
import { NMapComponent } from '../n-components/nMapComponent/n-map.component';
import { NLocaleResource } from '../n-services/n-localeResources.service';
import { NAuthGuardService } from 'neutrinos-seed-services';

window['neutrinos'] = {
  environments: environment
}

//CORE_REFERENCE_IMPORTS
//CORE_REFERENCE_IMPORT-buildgeneratorComponent
import { buildgeneratorComponent } from '../components/buildgeneratorComponent/buildgenerator.component';
//CORE_REFERENCE_IMPORT-modelhandlerService
import { modelhandlerService } from '../services/modelHandler/modelhandler.service';
//CORE_REFERENCE_IMPORT-computerbuilderComponent
import { computerbuilderComponent } from '../components/computerbuilderComponent/computerbuilder.component';
//CORE_REFERENCE_IMPORT-landingComponent
import { landingComponent } from '../components/landingComponent/landing.component';

/**
 * Reads datasource object and injects the datasource object into window object
 * Injects the imported environment object into the window object
 *
 */
export function startupServiceFactory(startupService: NDataSourceService) {
  return () => startupService.getDataSource();
}

/**
*bootstrap for @NgModule
*/
export const appBootstrap: any = [
  LayoutComponent,
];


/**
*Entry Components for @NgModule
*/
export const appEntryComponents: any = [
  //CORE_REFERENCE_PUSH_TO_ENTRY_ARRAY
];

/**
*declarations for @NgModule
*/
export const appDeclarations = [
  ImgSrcDirective,
  LayoutComponent,
  PageNotFoundComponent,
  NMapComponent,
  //CORE_REFERENCE_PUSH_TO_DEC_ARRAY
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-buildgeneratorComponent
buildgeneratorComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-computerbuilderComponent
computerbuilderComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-landingComponent
landingComponent,

];

/**
* provider for @NgModuke
*/
export const appProviders = [
  NDataSourceService,
  NLocaleResource,
  {
    // Provider for APP_INITIALIZER
    provide: APP_INITIALIZER,
    useFactory: startupServiceFactory,
    deps: [NDataSourceService],
    multi: true
  },
  NAuthGuardService,
  //CORE_REFERENCE_PUSH_TO_PRO_ARRAY
//CORE_REFERENCE_PUSH_TO_PRO_ARRAY-modelhandlerService
modelhandlerService,

];

/**
* Routes available for bApp
*/

// CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY_START
export const appRoutes = [{path: 'landing', component: landingComponent,
  children: [{ path: 'buildspage', component: buildgeneratorComponent },
{ path: 'componentAdd', component: computerbuilderComponent }]},
{path: '', redirectTo: 'landing/componentAdd', pathMatch: 'full'},
{path: '**', component: PageNotFoundComponent}]
// CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY_END
