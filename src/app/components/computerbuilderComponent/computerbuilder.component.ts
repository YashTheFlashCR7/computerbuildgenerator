/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
//services
import { modelhandlerService } from "../../services/modelHandler/modelhandler.service";

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-computerbuilder',
    templateUrl: './computerbuilder.template.html'
})

export class computerbuilderComponent extends NBaseComponent implements OnInit {
    components: any[]
    buildArray: any[]
    componentArray: any[]
    calculationArray: any[]
    BuildsModel: any[] = []
    BuildComponents: any[] = []
    //variables
    BuildCount: number
    isGeneratedBuilds: boolean
    isaddComponents:boolean
    constructor(private modelhandlerService: modelhandlerService) {
        super();

    }

    ngOnInit() {
        this.components = this.modelhandlerService.createComputerComponentDataModel()
        console.log(this.components)
        this.buildArray = this.modelhandlerService.createBuildDataModel()
        //form hide
        this.isGeneratedBuilds = false
        this.isaddComponents = true
    }

    onUnitTestClick(){
        this.components = this.modelhandlerService.createUnitTestModel()
    }
    addComponent(componentId: string) {
        let componentText = document.getElementById("input-" + componentId)
        let ComponentPrice = document.getElementById("price-" + componentId)
        let index = this.getIndex(componentId)
        if (componentText['value'] !== "" && ComponentPrice['value'] > 0) {
            //push into main data model
            this.components[index]['Addcomponents'].push(
                {
                    componentText: componentText['value'],
                    ComponentPrice: ComponentPrice['value']
                })

            //clear box
            componentText['value'] = ""
            ComponentPrice['value'] = ""
            //generate averages
            this.generateAverage(index)
            this.OrderBuildsByCheapestComponent()
        }


    }

    getIndex(componentId: string) {
        let index: number
        for (let i = 0; i < this.components['length']; i++) {
            if (this.components[i]['id'] === componentId) {
                index = i
                i = this.components['length']
            }
        }
        return index
    } //TODO: move to service 

    generateAverage(index: number) {
        let Total: number = 0
        let length: number = this.components[index]['Addcomponents']['length']
        for (let i = 0; i < length; i++) {
            Total = Total + parseInt(this.components[index]['Addcomponents'][i]['ComponentPrice'])
        }

        this.components[index]['average'] = Math.round(Total / length)
    } //TODO: move to service 

    createComponentModel(Component: any) {
        this.componentArray.push(Component)
    }

    createCalculationsModel(Total: number, ComponentAverage: number, budget6months: number, budget12months: number) {
        this.calculationArray.push({
            Total: Total,
            ComponentAverage: ComponentAverage,
            budget6months: budget6months,
            budget12months: budget12months
        });
    }

    OrderBuildsByCheapestComponent() {
        for (let i = (this.components.length - 1); i >= 0; i--) {
            if (this.components[i]['Addcomponents']['length'] > 0) {
                this.components[i]['Addcomponents'].sort((a, b) => (a.ComponentPrice - b.ComponentPrice))
            }
        }
        console.log(this.components)

    } //TODO: move to service 


    resetComponent(componentID: string) {
        let index = this.getIndex(componentID)
        this.components[index]['Addcomponents'] = []
        this.components[index]['average'] = 0
    }

    insertIntoBuildModel(buildId: string, buildDefinition: string, Components: any[]) {
        let Total = this.getTotalOfComponents(Components)
        this.BuildsModel.push({
            BuildID: buildId,
            BuildDefinition: buildDefinition,
            Components: Components,
            Rating: 5,
            Total:Total ,
            ComponentAverage:(Total/Components.length),
            budget6months: Math.round(Total/6),
            budget12months: Math.round(Total/12)
        })

        console.log('Builds model')
        console.log('=================')
        console.log(this.BuildsModel)
        console.log('=================')
    }  //TODO: move to service 


    generateCheapestBuild() {
        this.generateBuilds('CHP')
    } //TODO: ccoalesc into single methods ,  move to service 

    generateExpensiveBuild() {
        this.generateBuilds('EXP')
    }

    generateRandomBuilds() {
        this.generateBuilds('RND')
    }
    generateBuilds(context: string) {
        let component: any = []
        let index: number = 0
        let definition:string = ''
        this.components.forEach(element => {
            switch (context) {
                case 'CHP':
                    index = 0
                    definition = "cheapest build"
                    break;

                case 'EXP':
                    index = element['Addcomponents']['length'] - 1
                    definition = "most expensive build"
                    break;
                case 'RND':
                    definition = "Random build"
                    if (element['Addcomponents']['length'] > 1) {
                        do {
                            index = this.randomIntFromInterval(0, element['Addcomponents']['length'])
                        } while (index >= element['Addcomponents']['length']-1);
                        
                    }
                    break;
                default:
                    break;
            }
            if (element['Addcomponents']['length'] > 0 && index <= element['Addcomponents']['length'] - 1) {
                component.push({
                    componentText: element['Addcomponents'][index]['componentText'],
                    componentPrice: parseInt(element['Addcomponents'][index]['ComponentPrice']),
                    componentImage: element['image'],
                })
            }
        });
        this.insertIntoBuildModel(context + (this.BuildsModel['length'] + 1), definition, component)
    } //TODO: ccoalesc into single methods ,  move to service 


    randomIntFromInterval(min: number, max: number) { // min and max included 
        return Math.floor(Math.random() * (max - min + 1) + min);
    } //TODO:  move to service 

    getTotalOfComponents(components:any){
        let Total:number = 0
        for(let i=0;i<components.length;i++){
            Total= Total + parseInt(components[i]['componentPrice'])
        }
        return Total
    }

    coorindateBuilds() {
        this.generateCheapestBuild()
        this.generateExpensiveBuild()
        for(let i =0 ; i < 3 ; i++){
            this.generateRandomBuilds()
        }
        this.isGeneratedBuilds = true
        this.isaddComponents = false
    }

}
