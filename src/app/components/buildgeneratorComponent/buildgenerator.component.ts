/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit,Input } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
//services
import { modelhandlerService } from "../../services/modelHandler/modelhandler.service";

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-buildgenerator',
    templateUrl: './buildgenerator.template.html'
})

export class buildgeneratorComponent extends NBaseComponent implements OnInit {
    @Input() BuildsArray: any
    classBuildsArray: any
    buildsActive:boolean

    constructor(private modelhandlerService:modelhandlerService) {
        super();
    
    }

    ngOnInit() {
        if(this.BuildsArray){
          this.classBuildsArray = this.BuildsArray
          this.buildsActive = true
        }
        else{
          this.buildsActive = false
        }
        
        console.log(this.classBuildsArray)
    }

    formatLabel(value: number | null) {
        if (!value) {
          return 0;
        }
    
        if (value >= 1000) {
          return Math.round(value / 1000) + 'k';
        }
    
        return value;
      }

}
