export const environment = {
    "envId": "b5957805-db95-1c01-9ab0-31281802c00d",
    "name": "dev",
    "properties": {
        "production": false,
        "baseUrl": "http://localhost:3000/bhive-art/",
        "tenantName": "telesure",
        "appName": "computerBuildGenerator",
        "namespace": "com.telesure.computerBuildGenerator",
        "isNotificationEnabled": false,
        "googleMapKey": "AIzaSyCSTnVwijjv0CFRA4MEeS-H6PAQc87LEoU",
        "firebaseSenderId": "FIREBASE_SENDER_ID",
        "firebaseAuthKey": "FIREBASE_AUTH_KEY",
        "authDomain": "FIREBASE_AUTH_DOMAIN",
        "databaseURL": "FIREBASE_DATABASE_URL",
        "storageBucket": "FIREBASE_STORAGE_BUCKET",
        "appDataSource": "telesure-rt",
        "appAuthenticationStrategy": "basicAuth",
        "basicAuthUser": "username",
        "basicAuthPassword": "password",
        "useDefaultExceptionUI": true
    }
}