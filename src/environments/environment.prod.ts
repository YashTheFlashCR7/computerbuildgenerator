export const environment = {
    "envId": "389f1ec7-885a-fb16-552e-91b7b8267c79",
    "name": "prod",
    "properties": {
        "production": true,
        "baseUrl": "http://localhost:3000/bhive-art/",
        "tenantName": "telesure",
        "appName": "computerBuildGenerator",
        "namespace": "com.telesure.computerBuildGenerator",
        "isNotificationEnabled": false,
        "googleMapKey": "AIzaSyCSTnVwijjv0CFRA4MEeS-H6PAQc87LEoU",
        "firebaseSenderId": "FIREBASE_SENDER_ID",
        "firebaseAuthKey": "FIREBASE_AUTH_KEY",
        "authDomain": "FIREBASE_AUTH_DOMAIN",
        "databaseURL": "FIREBASE_DATABASE_URL",
        "storageBucket": "FIREBASE_STORAGE_BUCKET",
        "appDataSource": "telesure-rt",
        "appAuthenticationStrategy": "basicAuth",
        "basicAuthUser": "username",
        "basicAuthPassword": "password",
        "useDefaultExceptionUI": true
    }
}